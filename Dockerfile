FROM centos:7.7.1908

LABEL maintainer "benjamin.bertrand@esss.se"

RUN yum update -y \
  # Use IUS repository to install recent version of git (git2u)
  && yum -y install  https://repo.ius.io/ius-release-el7.rpm \
  && yum install -y \
    file \
    patch \
    tree \
    m4 \
    gcc-c++ \
    git224-all \
    expectk \
    tclx \
    graphviz \
    libtirpc-devel \
    re2c \
    readline-devel \
    hdf5-devel \
    libxml2-devel \
    libjpeg-turbo-devel \
    libtiff-devel \
    blosc-devel \
    netcdf-devel \
    opencv-devel \
    wget \
    sudo \
    python3 \
    patchelf \
    boost-devel \
    glib2-devel \
  && yum clean all

# cross-compiler version
ENV E3_CROSS_COMPILER_VERSION=v0.0.21 \
    E3_CROSS_COMPILER_KERNEL=2.6-4.14 \
    E3_CROSS_COMPILER_SHORT_SHA=dc17a023

# Install ifc14xx toolchain
ENV IFC14XX_TOOLCHAIN_SCRIPT ifc14xx-glibc-x86_64-ifc14xx-toolchain-ppc64e6500-toolchain-${E3_CROSS_COMPILER_KERNEL}-${E3_CROSS_COMPILER_SHORT_SHA}.sh
RUN wget --quiet -P /tmp https://artifactory.esss.lu.se/artifactory/yocto/toolchain/${E3_CROSS_COMPILER_VERSION}/${IFC14XX_TOOLCHAIN_SCRIPT} \
  && chmod a+x /tmp/${IFC14XX_TOOLCHAIN_SCRIPT} \
  && /tmp/${IFC14XX_TOOLCHAIN_SCRIPT} -y \
  && rm -f /tmp/${IFC14XX_TOOLCHAIN_SCRIPT}

# Install cct toolchain
ENV CCT_TOOLCHAIN_SCRIPT cct-glibc-x86_64-cct-toolchain-corei7-64-toolchain-${E3_CROSS_COMPILER_KERNEL}-${E3_CROSS_COMPILER_SHORT_SHA}.sh
RUN wget --quiet -P /tmp https://artifactory.esss.lu.se/artifactory/yocto/toolchain/${E3_CROSS_COMPILER_VERSION}/${CCT_TOOLCHAIN_SCRIPT} \
  && chmod a+x /tmp/${CCT_TOOLCHAIN_SCRIPT} \
  && /tmp/${CCT_TOOLCHAIN_SCRIPT} -y \
  && rm -f /tmp/${CCT_TOOLCHAIN_SCRIPT}

# Install google git repo
RUN wget -O /usr/local/bin/repo https://storage.googleapis.com/git-repo-downloads/repo \
  && chmod a+x /usr/local/bin/repo

# Add user that will be used by gitlab-ci to compile
# This user shall have the proper uid to have write access on E3 NFS share
# It shall match the uid and gid of the user defined in LDAP
ENV USERNAME gitlab-ci
RUN groupadd -r -g 100000 ${USERNAME} \
  && useradd --no-log-init -r -m -g ${USERNAME} -u 10043 ${USERNAME}

# Create /epics directory in the image so that we can test
# without mounting an external dir
RUN mkdir /epics && chown ${USERNAME}:${USERNAME} /epics

USER ${USERNAME}
WORKDIR /home/${USERNAME}

# Setup git
# Set color.ui to avoid google git repo to prompt for it
RUN git config --global user.name ${USERNAME} \
  && git config --global user.email ${USERNAME}@localhost.localdomain \
  && git config --global color.ui auto
